# Check every 5 minutes if network blocking should be enabled or disabled
*/5 * * * *	root	if [ -x /usr/sbin/debian-edu-update-netblock ]; then /usr/sbin/debian-edu-update-netblock auto > /dev/null; fi
# The next two entries could be useful during development
# Set timestamp for the cf-agent run
#*/30 * * * *	root	 /usr/bin/touch /tmp/d-e-c
# Run cf-agent in case of debian-edu-config package upgrade
#*/20 * * * *	root	if test /var/lib/dpkg/info/debian-edu-config.list -nt /tmp/d-e-c; then /usr/sbin/cf-agent -D installation; fi
#
