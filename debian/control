Source: debian-edu-config
Section: misc
Priority: optional
Maintainer: Debian Edu Developers <debian-edu@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>,
           Holger Levsen <holger@debian.org>,
           Mike Gabriel <sunweaver@debian.org>,
           Dominik George <natureshadow@debian.org>,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), debhelper
Build-Depends-Indep: po-debconf,
                     po4a,
                     help2man,
                     libfilesys-df-perl
Homepage: https://blends.debian.org/edu
Vcs-Browser: https://salsa.debian.org/debian-edu/debian-edu-config
Vcs-Git: https://salsa.debian.org/debian-edu/debian-edu-config.git

Package: debian-edu-config
Architecture: all
Depends: ${misc:Depends},
         adduser,
         bind9-host,
         cfengine3,
         debconf-utils,
         debian-edu-artwork,
         e2fsprogs,
         education-tasks,
         fping,
         gnutls-bin,
         iptables,
         isenkram-cli,
         ldap-utils,
         libconfig-inifiles-perl,
         libfilesys-df-perl,
         libglib2.0-bin,
         libhtml-fromtext-perl,
         libio-socket-ssl-perl,
         libnet-ldap-perl,
         libnet-netmask-perl,
         libnss3-tools,
         libpacparser1,
         libpam-python (>= 1.1.0~git20220701.1d4e111-0.3~),
         libproxy1-plugin-kconfig,
         libproxy1-plugin-webkit,
         libproxy1-plugin-networkmanager,
         libsitesummary-perl,
         libterm-readkey-perl,
         libtext-unaccent-perl,
         lockfile-progs,
         lsb-release,
         media-types,
         net-tools,
         ng-utils,
         openssl,
         patch,
         python3,
         python3-notify2,
         ssl-cert,
         swaks,
         tftp-hpa | tftp,
         uuid,
         wget,
Recommends: binutils,
            libnotify-bin,
            lsof,
            memtest86+,
            resolvconf,
            syslinux,
Description: Configuration files for Debian Edu (Skolelinux) systems
 Installs cfengine config files to be used by the machines set up as part
 of the Debian Edu (Skolelinux) network. Debian Edu is a Debian Pure Blend.
