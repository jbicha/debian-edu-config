# translation of nb.po to Norwegian Bokmål
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Petter Reinholdtsen <pere@hungry.com>, 2010, 2014.
msgid ""
msgstr ""
"Project-Id-Version: nb\n"
"Report-Msgid-Bugs-To: debian-edu-config@packages.debian.org\n"
"POT-Creation-Date: 2013-05-22 15:09+0200\n"
"PO-Revision-Date: 2014-08-19 22:23+0200\n"
"Last-Translator: Petter Reinholdtsen <pere@hungry.com>\n"
"Language-Team: Norwegian Bokmål <i18n-no@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../debian-edu-config.templates:1001
msgid "Should the init.d/update-hostname script run at boot time?"
msgstr "Skal skriptet init.d/update-hostname kjøre ved boot?"

#. Type: boolean
#. Description
#: ../debian-edu-config.templates:2001
msgid "Do you want to run enable-nat on your system?"
msgstr "Ønsker du å kjøre enable-nat på ditt system?"

#. Type: boolean
#. Description
#: ../debian-edu-config.templates:2001
msgid ""
"The enable-nat script activates NAT for your Thin-Clients and overwrites "
"your iptables rules."
msgstr ""
"Scriptet enable-nat aktiverer NAG for dine tynne klienter og overskriver "
"dine iptables-regler."

#. Type: password
#. Description
#: ../debian-edu-config.templates:4001
msgid "Enter the Kerberos KDC master key:"
msgstr "Skriv inn hovednøkkel for Kerberos KDC:"

#. Type: password
#. Description
#: ../debian-edu-config.templates:4001
msgid ""
"A password is needed as Kerberos master key and for all default principals.  "
"You can use your root password or type something else. Make sure you "
"remember the password."
msgstr ""
"Det trengs et passord som hovednøkkel for Kerberos og alle default-aktører.  "
"Du kan bruke root-passordet ditt eller skrive inn noe annet.  Pass på at du "
"husker passordet."

#. Type: password
#. Description
#. Type: password
#. Description
#: ../debian-edu-config.templates:4001 ../debian-edu-config.templates:9001
msgid "Note that you will not be able to see the password as you type it."
msgstr "Merk at du ikke vil kunne se passordet når du skriver det inn."

#. Type: password
#. Description
#. Type: password
#. Description
#: ../debian-edu-config.templates:5001 ../debian-edu-config.templates:10001
msgid "Re-enter password to verify:"
msgstr "Skriv passordet på nytt for å bekrefte:"

#. Type: password
#. Description
#. Type: password
#. Description
#: ../debian-edu-config.templates:5001 ../debian-edu-config.templates:10001
msgid ""
"Please enter the same password again to verify that you have typed it "
"correctly."
msgstr ""
"Skriv inn det samme passordet på nytt for å bekrefte at du har skrevet det "
"inn korrekt."

#. Type: error
#. Description
#. Type: error
#. Description
#: ../debian-edu-config.templates:6001 ../debian-edu-config.templates:11001
msgid "Password input error"
msgstr "Feil med passordinnskriving"

#. Type: error
#. Description
#. Type: error
#. Description
#: ../debian-edu-config.templates:6001 ../debian-edu-config.templates:11001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "De to passordene du skrev inn er ikke like.  Forsøk igjen."

#. Type: error
#. Description
#. Type: error
#. Description
#: ../debian-edu-config.templates:7001 ../debian-edu-config.templates:12001
msgid "Empty password"
msgstr "Tomt passord"

#. Type: error
#. Description
#. Type: error
#. Description
#: ../debian-edu-config.templates:7001 ../debian-edu-config.templates:12001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Du skrev inn et tomt passord, hvilket ikke er tillatt.  Velg et ikke-tomt "
"passord."

#. Type: password
#. Description
#: ../debian-edu-config.templates:9001
msgid "Enter the LDAP super-admin password:"
msgstr "Skriv inn LDAPs super-admin-passord:"

#. Type: password
#. Description
#: ../debian-edu-config.templates:9001
msgid ""
"A password is used as initial password for the super-admin user of GOsa². "
"You can use your root password or type something else. Make sure you "
"remember the password."
msgstr ""
"Et passord brukes som utgangspassord for brukeren super-admin i GOsa².  Du "
"kan bruke root-passordet ditt eller skrive inn noe annet.  Pass på at du "
"husker passordet."

