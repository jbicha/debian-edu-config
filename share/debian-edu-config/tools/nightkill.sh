#!/bin/bash

# $Id$
# This script could be run every night, to kill stray processes. 

KEEPONES="root USER daemon"
USERLIMIT=10000

if [ -f /etc/debian-edu/nightkill.conf ] ; then 
  . /etc/debian-edu/nightkill.conf
fi 

for EVERY in $KEEPONES ; do 
  KEEP="${KEEP:+$KEEP|}^$EVERY"
done


ps hauxwn | \
  while read PUSER PID NULL ; do 
    [ $PUSER -ge $USERLIMIT ] && kill $PID >& /dev/null
  done

sleep 10
  
ps hauxwn  | \
  while read PUSER PID NULL ; do 
    [ $PUSER -ge $USERLIMIT ] && kill -9 $PID >& /dev/null 
  done
