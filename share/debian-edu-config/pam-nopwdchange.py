#!/usr/bin/python3
#
# Reject password change, ask people to use the Gosa web interface
# instead.

from __future__ import absolute_import
from __future__ import print_function
import sys
import syslog
import pwd

def pam_sm_setcred(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_authenticate(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_acct_mgmt(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_open_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_close_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_chauthtok(pamh, flags, argv):
  syslog.openlog("pam_edu_nopwdchange", syslog.LOG_PID, syslog.LOG_AUTH)
  syslog.syslog("calling pam_sm_chauthtok()")
  user = pamh.get_user(None)
  userinfo = pwd.getpwnam(user)
  uid = userinfo[2]
  if 2000 <= uid:
    text = "\nPlease visit https://www/gosa to change your password for Debian Edu / Skolelinux. Thanks!\n"
    msg = pamh.Message(pamh.PAM_TEXT_INFO, text)
    pamh.conversation(msg)
    syslog.syslog("rejected password change for user %s" % user)
    return pamh.PAM_SYSTEM_ERR
  return pamh.PAM_SUCCESS

# Test if the code work.  Argument is username to simulate login for.
if __name__ == '__main__':
  syslog.openlog("pam_nopwdchange", syslog.LOG_PID, syslog.LOG_AUTH)
  user = sys.argv[1]
  class pam_handler:
    PAM_SUCCESS = 1
    PAM_USER_UNKNOWN = 2
    PAM_SYSTEM_ERR = 3
    PAM_TRY_AGAIN = 4
    PAM_TEXT_INFO = 5
    def get_user(self, arg):
      return user
    def Message(self, tag, str):
      return str
    def conversation(self, msg):
      print("PAM conversation: " + msg)
      return
  pamh = pam_handler()
  if pamh.PAM_SUCCESS == pam_sm_chauthtok(pamh, None, None):
    print("pam_sm_chauthtok returned PAM_SUCCESS")
  else:
    print("pam_sm_chauthtok returned PAM_SYSTEM_ERR")
