// Force proxy usage.  Only for networked clients. Enabled using cfengine
// by symlinking this file to /etc/firefox-esr/debian-edu-networked.js

// Enable automatic proxy setting Configure Proxy settings in firefox,
// using "Web Access Protocol Discovery" (WAPD).  See
// /etc/debian-edu/www/wpad.dat for the WAPD files.  The location of
// The WPAD file is handed out using DHCP and firefox should look for
// http://wpad/wpad.dat automatically.
pref("network.proxy.type", 4, locked);
