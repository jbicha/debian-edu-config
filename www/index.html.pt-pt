<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-pt">
  <head>
    <title>Bem vindo a &laquo;www&raquo;: Página de informações para uma instalação de
Debian-Edu</title>
    <link rev="made" href="mailto:linuxiskolen@skolelinux.no"/>
    <link rel="top" href="https://wiki.debian.org/DebianEdu"/>
    <link rel="alternate" title="norsk" href="index.html.nb-no"/>
    <link rel="alternate" title="English" href="index.html.en"/>
    <link rel="alternate" title="dansk" href="index.html.da"/>
    <link rel="alternate" title="Deutsch" href="index.html.de"/>
    <link rel="alternate" title="français" href="index.html.fr"/>
    <link rel="alternate" title="català" href="index.html.ca"/>
    <link rel="alternate" title="español" href="index.html.es-es"/>
    <link rel="alternate" title="Indonesia" href="index.html.id"/>
    <link rel="alternate" title="Italiano" href="index.html.it"/>
    <link rel="alternate" title="日本語" href="index.html.ja"/>
    <link rel="alternate" title="Nederlands" href="index.html.nl"/>
    <link rel="alternate" title="Português" href="index.html.pt-pt"/>
    <link rel="alternate" title="Português do Brasil" href="index.html.pt-br"/>
    <link rel="alternate" title="Română" href="index.html.ro"/>
    <link rel="alternate" title="Русский" href="index.html.ru"/>
    <link rel="alternate" title="中文" href="index.html.zh-tw"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Language" content="pt-pt"/>
    <meta name="Author" content="Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"/>
    <link rel="stylesheet" href="skl-ren_css.css" type="text/css"/>
  </head>

  <body>

    <div class="hbar"/>

    <div class="menu">

      <h2 class="menu">Serviços Locais</h2>
     <ul>
      <li><a href="/debian-edu-doc/en/">Documentação</a></li>
      <li><a href="/gosa/">GOsa<sup>2</sup> Administração do LDAP</a></li>
      <li><a href="https://www.intern:631/">Administração de Impressoras</a></li>
      <li><a href="/slbackup-php">Salvaguarda</a></li>
      <li><a href="/icingaweb2/">Icinga</a></li>
      <li><a href="/munin/">Munin</a></li>
      <li><a href="/sitesummary/">Sumário do Site</a></li>
     </ul>

      <h2 class="menu">Debian Edu</h2>
     <ul>
      <li><a href="https://blends.debian.org/edu/">Página Web</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu">Página Wiki</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu/MailingLists">Listas de Email</a></li>
      <li><a href="https://popcon.debian.org/">Utilização de pacotes recolhida</a></li>
     </ul>

    </div>

    <div class="body">

    <div align="left">

      <!-- Note to translators: these strings should not be translated -->
<a href="index.html.ca">[català]</a> <a href="index.html.da">[dansk]</a> <a
href="index.html.de">[Deutsch]</a> <a href="index.html.en">[English]</a> <a
href="index.html.es-es">[español]</a> <a href="index.html.fr">[français]</a>
<a href="index.html.id">[Indonesia]</a> <a
href="index.html.it">[Italiano]</a> <a href="index.html.nb-no">[norsk]</a>
<a href="index.html.nl">[Nederlands]</a> <a
href="index.html.pt-pt">[Português]</a> <a
href="index.html.pt-br">[Português do Brasil]</a> <a
href="index.html.ro">[Română]</a> <a href="index.html.ru">[Русский]</a> <a
href="index.html.zh-tw">[中文]</a> <a href="index.html.ja">[日本語]</a>
    </div>


    <h1><a name="top"><img src="logo-trans.png" alt="Skolelinux"/></a></h1>

    <h2>Bem vindo ao Debian Edu / Skolelinux</h2>
    <p><strong>Se você consegue ver isto, significa que a instalação do seu
servidor Debian-edu teve sucesso. Parabéns e vem vindo. Para mudar o
conteúdo desta página, edite /etc/debian-edu/www/index.html.pt-pt, com o seu
editor favorito.</strong></p>
    <p>
    No lado direito desta página você vê alguns links que podem ser úteis para
si e para o seu trabalho, administrando uma rede Debian Edu.
    </p>
    <ul>
     <li>Os links sob serviços locais são links para serviços em execução neste
servidor. Estas ferramentas podem ajudá-lo no seu trabalho diário com a
solução Debian Edu.
    </li>
     <li>Os links sob Debian-Edu são links para páginas do Debian-Edu e/ou do
Skolelinux na Internet.
     <ul>
      <li><strong>Documentação:</strong> Escolha isto para explorar a documentação
instalada.</li>
      <li><strong>GOsa<sup>2</sup> Administração de LDAP:</strong> Escolha isto para
ir para GOsa<sup>2</sup> Sistema web de administração LDAP. Use isto para
adicionar e editar utilizadores e máquinas.</li>
      <li><strong>Administração de Impressora:</strong> Escolha isto para administrar
as suas impressoras.</li>
      <li><strong>Salvaguarda:</strong> Escolha isto para obter o sistema de
salvaguarda, aqui você pode restaurar ou alterar a salvaguarda (backup)
nocturna.</li>
      <li><strong>Icinga:</strong> Escolha isto para chegar às páginas de Icinga
monitorização do sistema.</li>
      <li><strong>Munin:</strong> Escolha isto para chagar ás páginas de estatísticas
Munin.</li>
      <li><strong>Sumário do Site</strong> Escolha isto para chegar ao relatório
sumário das máquinas de rede de Debian Edu.</li> 
     </ul>
     </li>
    </ul>
    <h2>Páginas web pessoais para os utilizadores do sistema.</h2>
    <p>Todos os utilizadores no sistema, podem criar um catálogo no seu directório
home com o nome &laquo;public_html&raquo;. Aqui os utilizadores podem
adicionar as suas páginas pessoais. As páginas pessoais estarão disponíveis
no endereço https://www/~nome-utilizador/. Se você tiver um utilizador
chamado Jon Doe, com o nome de utilizador jond, a sua página web estará
disponível em  <a href="https://www/~jond/">https://www/~jond/</a>. Se o
utilizador não existir, ou se o catálogo public_html não existir, você irá
obter a página de erro &laquo;Not Found&raquo;. Se o catálogo public_html
existir, mas estiver vazio, você vai obter a página de erro
&laquo;Permission Denied&raquo;. Para mudar isto, altere o ficheiro
index.html dentro do catálogo public_html.</p>

    </div>
  </body>
</html>
