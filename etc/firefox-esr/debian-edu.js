// Limit the disk cache, and disable it by default, to avoid users
// filling up their home directory with cache files.
pref("browser.cache.disk.enable", false);
pref("browser.cache.offline.enable", false);
pref("browser.cache.disk.capacity", 5120);

// Only cache to memory instead
pref("browser.cache.memory.enable", true);
pref("browser.cache.memory.max_entry_size", -1);

// Disable storing website screenshots on disk
pref("browser.pagethumbnails.capturing_disabled", true);

// Enable spell checking in both single-line and multi-line fields
pref("layout.spellcheckDefault", 2);

// Mailto settings
pref("network.protocol-handler.app.mailto", "thunderbird");

// Disable malware and other crap detection to avoid heavy I/O during login.
// This should be no problem because ublock-origin is installed and the system
// can be kept up-to-date easily.
pref("browser.safebrowsing.phishing.enable", false);
pref("browser.safebrowsing.malware.enabled", false);
pref("browser.safebrowsing.blockedURIs.enabled", false);
pref("browser.safebrowsing.downloads.enabled", false);
pref("browser.safebrowsing.downloads.remote.block_dangerous", false);
pref("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
pref("browser.safebrowsing.downloads.remote.url", "");
pref("browser.safebrowsing.provider.*.gethashURL", "");
pref("browser.safebrowsing.provider.*.updateURL", "");

// Disable telemetry data reporting and toolkit.
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("toolkit.telemetry.unified", false);
// Also disable the telemetry coverage toolkit, just in case it is present.
pref("toolkit.coverage.endpoint.base", "");
pref("toolkit.coverage.opt-out", true);
pref("toolkit.telemetry.coverage.opt-out", true);

// Disable location tracking
pref("browser.region.update.enabled", false);
pref("browser.region.network.url", "");

// Avoid connecting to Mozilla after upgrades
pref("browser.startup.homepage_override.mstone", "ignore");

// Avoid connecting to Mozilla at each startup
pref("app.normandy.enabled", false);

// Disable screenshot uploads
pref("extensions.screenshots.upload-disabled", true);


// Disable location-bar suggestion feature that is sludgy on
// thin-clients.
pref("browser.urlbar.maxRichResults", 0);

// Tell Firefox to not look for upgrades.  Use apt to upgrade.
pref("app.update.enabled", false);

// But it is allowed to look for new extentions.
pref("extensions.update.enabled", true);

// Use LANG environment variable to choose locale.
pref("intl.locale.matchOS", true);

// Disable default browser checking.
pref("browser.shell.checkDefaultBrowser", false);

// Use clean new tab page without any distraction except search.
pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
pref("browser.newtabpage.activity-stream.feeds.topsites", false);
pref("browser.newtabpage.activity-stream.feeds.snippets", false);
pref("browser.newtabpage.activity-stream.feeds.asrouterfeed", false);
