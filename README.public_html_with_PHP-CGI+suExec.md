# Debian Edu and PHP scripts in ~/public_html

This is about the Apache2 configuration on the Debian Edu mainserver (aka
``tjener.intern``).

## Security Notices and Warnings

The configuration file
``/etc/apache2/mods-available/debian-edu-userdir.conf`` disables the
Apache2 built-in PHP support for PHP scripts in user directory (i.e. in
web pages served via ~/public_html).

This is for a reason, as Apache2's default system account ``www-data``
has certain privileges on the Debian Edu main-server that we don't want to
expose to arbitrary code in some ``~/public_html`` directory.

If people shall be able to execute PHP (or other CGI scripts) from
``~/public_html`` for studying purposes, then they should be executed
with the script file owner's user privileges.

Do not use this method if it is not required to interpret PHP code from
within ~/public_html directories. This method (opposed to Apache2's
built-in PHP support) exposes user home directories to data manipulation
risks (in case users create bad code in their ~/public_html scripts).
Also, this method turns Apache2's PHP interpretation into some rather
performance wasting mode (libapach2-mod-phpX -> phpX-cgi).

## Test built-in PHP

Here is a simple PHP script for testing PHP interpretation privileges and
checking later on that the suExec'ing really works:

```
<?php

print "hello, this script runs as user '";
system("id -u -n");
print "'<br>";

?>
```

Place this script as e.g. ~/public_html/id.php into your HOME directory and
make sure that everyone can access this file. A restrictive permission example
is this:

```
<user>@tjener:~/public_html$ ls -al
insgesamt 12
drwx-----x  2 <user> <primgroup> 4096 19. Jan 20:35 .
drwx-----x 20 <user> <primgroup> 4096 19. Jan 20:35 ..
-rw----r--  1 <user> <primgroup>   90 19. Jan 20:35 id.php
```

Now edit /etc/apache2/mods-available/debian-edu-userdir.conf on TJENER and
set ``php_admin_flag engine on``.

When opening the URL ``http://www.intern/~<user>/id.php`` it should show this simple web page:

```
hello, this script runs as user 'www-data'
```

As enabling the built-in PHP engine (running as user www-data) is
considered insecure on the Debian Edu main-server (aka TJENER), here
follows the promised alternative setup.

## Enable PHP CGI with suExec

Install Apache2, PHP, suExec and support for executing pseudo-binaries.

```
<user>@tjener:~/public_html$ sudo apt-get install apache2-suexec-pristine php-cgi binfmt-support
```

Configure PHP to run ``.php`` scripts from the shell.

```
<user>@tjener:~/public_html$ sudo update-binfmts --install PHP /usr/bin/php-cgi --extension php
```

Enable the necessary suExec Apache module:

```
<user>@tjener:~/public_html$ sudo a2enmod suexec
```

Re-configure the ``debian-edu-userdir.conf`` module as follows:

```
--- a/etc/apache2/mods-available/debian-edu-userdir.conf
+++ b/etc/apache2/mods-available/debian-edu-userdir.conf
@@ -8,6 +8,12 @@
                # Read /usr/share/doc/debian-edu-config/README.public_html_with_PHP-CGI.md
                php_admin_flag engine off
 
+               Options +ExecCGI
+               <FilesMatch "\.php$">
+                       SetHandler cgi-script
+                       SetEnv REDIRECT_STATUS 1
+               </FilesMatch>
+
                AllowOverride FileInfo AuthConfig Limit
                Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec
                <Limit GET POST OPTIONS>
```

Reload the Apache configuration.

```
<user>@tjener:~/public_html$ sudo /etc/init.d/apache2 force-reload
```

## User Permissions on userdir PHP files

Finally, userdir PHP scripts in ``~/public_html`` must not be writeable
by group or others. If otherwise, their execution gets blocked by
Apache2.

Also, their executable bit must be set.

See example below:

```
<user>@tjener:~/public_html$ find ~/public_html -name "*.php" -exec chmod 0700 {} \;
```

This will turn our ``~/public_html`` test directory to:

```
<user>@tjener:~/public_html$ ls -al
insgesamt 12
drwx-----x  2 <user> <primgroup> 4096 19. Jan 20:35 .
drwx-----x 20 <user> <primgroup> 4096 19. Jan 20:35 ..
-rwx------  1 <user> <primgroup>   90 19. Jan 20:35 id.php

```

When opening the URL ``http://www.intern/~<user>/id.php`` now, it should
show the owning user as account this script has been run under:

```
hello, this script runs as user '<user>'
```
