PACKAGE = debian-edu-config

NULL =

PROGS = \
	debian-edu-ldapserver \
	update-ini-file \
	debian-edu-copy-pki \
	$(NULL)

SPROGS = \
	debian-edu-fsautoresize \
	debian-edu-ltsp-chroot \
	debian-edu-ltsp-install \
	debian-edu-ltsp-initrd \
	debian-edu-ltsp-ipxe \
	debian-edu-pxeinstall \
	debian-edu-restart-services \
	debian-edu-test-install \
	debian-edu-update-netblock \
	update-hostname-from-ip \
	$(NULL)

LIBEXECPROGS = \
	debian-edu-cups-queue-autoflush-for-netgroup-hosts \
	debian-edu-cups-queue-autoreenable-for-netgroup-hosts \
	debian-edu-fsautoresize-for-netgroup-hosts \
	$(NULL)

INSTALL        = install -D -p -m 755
INSTALL_DATA   = install -D -p -m 644

prefix         = /usr/local
sysconfdir     = /etc
cf3dir         = $(sysconfdir)/cfengine3/debian-edu
bindir         = $(prefix)/bin
sbindir        = $(prefix)/sbin
docdir         = $(prefix)/share/doc/$(PACKAGE)
mandir         = $(prefix)/share/man
ldapdir        = $(sysconfdir)/ldap
slbackupphpdir = $(sysconfdir)/slbackup-php
schemadir      = $(ldapdir)/schema
dhcpdir        = $(sysconfdir)/dhcp
libdir         = /usr/lib
pkglibdir      = $(libdir)/debian-edu-config
libexecdir     = /usr/libexec
pkglibexecdir  = $(libexecdir)/debian-edu-config
vardir         = /var
wwwdir         = /etc/debian-edu/www


CF3FILES = \
	cf.adduser \
	cf.apache2 \
	cf.cfengine3 \
	cf.cups \
	cf.desktop-networked \
	cf.dhcpserver \
	cf.exim \
	cf.imap \
	cf.homes \
	cf.firefox-esr \
	cf.finalize \
	cf.grub \
	cf.chromium \
	cf.inetd \
	cf.krb5client \
	cf.ldapserver \
	cf.ldapclient \
	cf.bind \
	cf.pam \
	cf.pxeinstall \
	cf.ntp \
	cf.samba \
	cf.squid \
	cf.sshd \
	cf.syslog \
	cf.workarounds \
	cf.xrdp \
	cf.icinga \
	edu.cf \
	promises.cf \
	$(NULL)

# Files to install in /etc/
SYSCONFFILES = \
	apt/apt.conf.d/99-edu-prefer-firefox \
	bind/named.conf.ldap2zone \
	bind/db.intern \
	bind/db.10.in-addr.arpa. \
	bind/db.subnet00.intern \
	bind/db.subnet01.intern \
	bind/db.0.168.192.in-addr.arpa. \
	bind/db.1.168.192.in-addr.arpa. \
	X11/Xsession.d/05debian-edu-truncate-xerrorlog \
	X11/Xsession.d/09debian-edu-missing-home \
	X11/Xsession.d/10debian-edu-one-login-per-host \
	X11/Xsession.d/55lightdm_gtk-greeter-rc \
	dconf/profile/user \
	debian-edu/nightkill.conf \
	debian-edu/pxeinstall.conf \
	default/munin-node \
	default/ldap2zone \
	chromium/policies/managed/chromium-networked-prefs \
	chromium/policies/recommended/search_provider.json \
	cups/cupsd-debian-edu.conf \
	cups/cups-files-debian-edu.conf \
	cups/cups-browsed-debian-edu.conf \
	dhcp/dhcpd-debian-edu.conf \
	dhcp/dhclient-debian-edu.conf \
	dovecot/local.conf \
	exim4/exim-ldap-client-v4.conf \
	exim4/exim-ldap-server-v4.conf \
	exports.d/edu.exports \
	filesystems \
	firefox-esr/debian-edu.js \
	php/apache2/php-debian-edu.ini \
	ldap/rootDSE-debian-edu.ldif \
	ldap/slapd-debian-edu-mdb.conf \
	samba/smb-debian-edu.conf \
	slbackup-php/config.php \
	sssd/sssd-debian-edu.conf \
	xdg/autostart/welcome-webpage.desktop \
	lsb-release \
	apache2/mods-available/debian-edu-userdir.conf \
	apache2/sites-available/debian-edu-default.conf \
	apache2/sites-available/debian-edu-ssl-default.conf \
	apache2/conf-available/debian-edu-config-doc.conf \
	apache2/include/debian-edu-ldapauth.inc \
	nagios3/debian-edu/cgi.cfg \
	nagios3/debian-edu/commands.cfg \
	nagios3/debian-edu/contactgroups.cfg \
	nagios3/debian-edu/contacts.cfg \
	nagios3/debian-edu/dependencies.cfg \
	nagios3/debian-edu/htpasswd.users \
	nagios3/debian-edu/escalations.cfg \
	nagios3/debian-edu/hostextinfo.cfg \
	nagios3/debian-edu/hostgroups.cfg \
	nagios3/debian-edu/hosts.cfg \
	nagios3/debian-edu/host_templates.cfg \
	nagios3/debian-edu/nagios.cfg \
	nagios3/debian-edu/resources.cfg \
	nagios3/debian-edu/serviceextinfo.cfg \
	nagios3/debian-edu/servicegroups.cfg \
	nagios3/debian-edu/services.cfg \
	nagios3/debian-edu/service_templates.cfg \
	nagios3/debian-edu/timeperiods.cfg \
	munin/debian-edu-munin-node.conf \
	polkit-1/localauthority.conf.d/80-edu-admin.conf \
	ntpsec/ntp.d/debian-edu.conf \
	$(NULL)

SYSCONFSCRIPTS = \
	dhcp/dhclient-exit-hooks.d/autofs-reload \
	dhcp/dhclient-exit-hooks.d/wpad-proxy-update \
	dhcp/dhclient-exit-hooks.d/fetch-rootca-cert \
	dhcp/dhclient-exit-hooks.d/hostname \
	mklocaluser.d/20-debian-edu-config \
	shutdown-at-night/clients-generator \
	resolvconf/update.d/bind-debian-edu \
	wicd/scripts/preconnect/set_wireless_mac_from_eth0 \
	X11/Xsession-debian-edu \
	$(NULL)

SCHEMAS = \
	autofs-debian-edu.schema \
	samba.schema \
	dhcp.schema \
	dnsdomainaux.schema \
	ltspclientaux.schema \
	eduorg-200210-openldap.schema \
	eduperson-200806-openldap.schema \
	kerberos.schema \
	noreduperson-1.5-openldap.schema \
	samba3.schema \
	sudo.schema \
	trust.schema \
	gosystem.schema \
	gofon.schema \
	goto.schema \
	gosa-samba3.schema \
	gofax.schema \
	goserver.schema \
	goto-mime.schema \
	$(NULL)

LDIFS = \
	root.ldif \
	autofs.ldif \
	firstuser.ldif \
	ipnetworks.ldif \
	netgroup.ldif \
	samba.ldif \
	sudo.ldif \
	krb5.ldif \
	ltsp.ldif \
	gosa.ldif \
	gosa-server.ldif \
	$(NULL)

LDAPPROGRAMS = \
	ldap-add-host-to-netgroup \
	ldap-add-user-to-group \
	ldap-createuser-krb5 \
	ldap2netgroup \
	ldap-debian-edu-install \
	sitesummary2ldapdhcp \
	$(NULL)

WWWFILES = \
	index.html.ca \
	index.html.da \
	index.html.de \
	index.html.en \
	index.html.es-es \
	index.html.fr \
	index.html.id \
	index.html.it \
	index.html.ja \
	index.html.nb-no \
	index.html.nl \
	index.html.pt-pt \
	index.html.pt-br \
	index.html.ro \
	index.html.ru \
	index.html.zh-tw \
	skl-ren_css.css \
	logo-trans.png \
	wpad.dat \
	$(NULL)

LIBFILES = \
	thunderbird/distribution/policies.json \
	$(NULL)

all:
	$(MAKE) -C www

install: install-testsuite
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(sbindir)
	install -d $(DESTDIR)$(sysconfdir) $(DESTDIR)$(sysconfdir)/cups
	install -d $(DESTDIR)$(ldapdir)
	install -d $(DESTDIR)$(dhcpdir)
	install -d $(DESTDIR)$(libdir)
	install -d $(DESTDIR)$(pkglibexecdir)

# program's manpages are autodetected. 
	set -e ; for prog in $(PROGS); do \
		$(INSTALL) bin/$$prog $(DESTDIR)$(bindir) ; \
		if [ -e "share/man/man1/$$prog.1" ]; \
		then \
		$(INSTALL_DATA) "share/man/man1/$$prog.1" $(DESTDIR)$(mandir)/man1/$$prog.1 ; \
		fi \
	done
# Using manpages autodetection : 
	set -e ; for sprog in $(SPROGS); do \
		$(INSTALL) sbin/$$sprog $(DESTDIR)$(sbindir) ; \
		if [ -e "share/man/man8/$$sprog.8" ]; \
		then \
		$(INSTALL_DATA) "share/man/man8/$$sprog.8" $(DESTDIR)$(mandir)/man8/$$sprog.8 ; \
		fi \
	done

	set -e ; for libexecprog in $(LIBEXECPROGS); do \
		$(INSTALL) libexec/$$libexecprog $(DESTDIR)$(pkglibexecdir) ; \
	done

	$(INSTALL_DATA) README $(DESTDIR)$(docdir)/README
	$(INSTALL_DATA) README.public_html_with_PHP-CGI+suExec.md $(DESTDIR)$(docdir)/README.public_html_with_PHP-CGI+suExec.md

	set -e ; for cf3 in $(CF3FILES); do \
		$(INSTALL_DATA) cf3/$$cf3 $(DESTDIR)$(cf3dir)/$$cf3; \
	done

	set -e ; for file in $(SYSCONFFILES) ; do \
		$(INSTALL_DATA) etc/$$file $(DESTDIR)$(sysconfdir)/$$file; \
	done

	mkdir -p $(DESTDIR)$(sysconfdir)/init.d
	set -e ; for file in $(SYSCONFSCRIPTS) ; do \
		$(INSTALL) etc/$$file $(DESTDIR)$(sysconfdir)/$$file; \
	done

	set -e ; for file in $(LIBFILES) ; do \
		$(INSTALL_DATA) lib/$$file $(DESTDIR)$(libdir)/$$file; \
	done

	set -e ; for f in \
		share/debian-edu-config/d-i/finish-install \
		share/debian-edu-config/d-i/pre-pkgsel \
		share/debian-edu-config/killer.cron \
		share/debian-edu-config/tools/passwd \
		share/debian-edu-config/tools/clean-up-host-keytabs \
		share/debian-edu-config/tools/create-debian-edu-certs \
		share/debian-edu-config/tools/create-server-cert \
		share/debian-edu-config/tools/cups-queue-autoflush \
		share/debian-edu-config/tools/cups-queue-autoreenable \
		share/debian-edu-config/tools/setup-cfengine3 \
		share/debian-edu-config/tools/debian-edu-bless \
		share/debian-edu-config/tools/get-default-homepage \
		share/debian-edu-config/tools/gosa-create \
		share/debian-edu-config/tools/gosa-create-host \
		share/debian-edu-config/tools/gosa-modify-host \
		share/debian-edu-config/tools/gosa-remove-host \
		share/debian-edu-config/tools/gosa-lock-user \
		share/debian-edu-config/tools/gosa-remove \
		share/debian-edu-config/tools/gosa-sync \
		share/debian-edu-config/tools/gosa-sync-dns-nfs \
		share/debian-edu-config/tools/gosa-unlock-user \
		share/debian-edu-config/tools/goodbye-user-session \
		share/debian-edu-config/tools/firefox-plugin-support \
		share/debian-edu-config/tools/kerberos-kdc-init \
		share/debian-edu-config/tools/ldap2bind-updatezonelist \
		share/debian-edu-config/tools/ldap-user-clean-attic.sh \
		share/debian-edu-config/tools/ldapdump.sh \
		share/debian-edu-config/tools/ldappasswd2 \
		share/debian-edu-config/tools/locate-syslog-collector \
		share/debian-edu-config/tools/logoutkill.sh \
		share/debian-edu-config/tools/ltsp-addfirmware \
		share/debian-edu-config/tools/missing-desktop-file \
		share/debian-edu-config/tools/movehome \
		share/debian-edu-config/tools/nightkill.sh \
		share/debian-edu-config/tools/notify-local-users \
		share/debian-edu-config/tools/package-disk-usage \
		share/debian-edu-config/tools/pipegraph \
		share/debian-edu-config/tools/preseed-ldap-kerberos \
		share/debian-edu-config/tools/preseed-sitesummary \
		share/debian-edu-config/tools/pxe-addfirmware \
		share/debian-edu-config/tools/run-at-firstboot \
		share/debian-edu-config/tools/setup-ad-client \
		share/debian-edu-config/tools/setup-roaming \
		share/debian-edu-config/tools/setup-freeradius-server \
		share/debian-edu-config/tools/show-welcome-webpage \
		share/debian-edu-config/tools/sssd-generate-config \
		share/debian-edu-config/tools/squid-update-cachedir \
		share/debian-edu-config/tools/subnet-change \
		share/debian-edu-config/tools/update-cert-dbs \
		share/debian-edu-config/tools/update-dlw-krb5-keytabs \
		share/debian-edu-config/tools/update-firefox-homepage \
		share/debian-edu-config/tools/update-chromium-homepage \
		share/debian-edu-config/tools/update-proxy-from-wpad \
		share/debian-edu-config/tools/wpad-extract \
		share/debian-edu-config/tools/ldap-server-getcert \
		share/debian-edu-config/tools/exim4-create-environment \
		share/debian-edu-config/tools/edu-ldap-from-scratch \
		share/debian-edu-config/tools/edu-icinga-setup \
		share/debian-edu-config/tools/create-user-nssdb \
		share/debian-edu-config/tools/copy-host-keytab \
		share/debian-edu-config/tools/improve-desktop-l10n \
		share/debian-edu-config/tools/install-task-pkgs \
		share/debian-edu-config/tools/chromium-ldapconf \
		share/debian-edu-config/tools/firefox-ldapconf \
		share/debian-edu-config/tools/nat \
		share/debian-edu-config/tools/fetch-rootca-cert \
	; do \
		$(INSTALL) $$f $(DESTDIR)/usr/$$f ; \
	done

	$(INSTALL_DATA) sbin/debian-edu-fsautoresizetab $(DESTDIR)/usr/share/debian-edu-config/fsautoresizetab

	set -e ; for f in \
		share/debian-edu-config/avahi.smb.service \
		share/debian-edu-config/rsyslog-collector \
		share/debian-edu-config/rsyslog-filters \
		share/debian-edu-config/smb.conf.edu-site \
		share/debian-edu-config/firefox-networked-prefs.js \
		share/debian-edu-config/squid.conf \
		share/debian-edu-config/ssl.cnf \
		share/debian-edu-config/sslCA.cnf \
		share/debian-edu-config/v3.cnf \
		share/debian-edu-config/v3CA.cnf \
		share/debian-edu-config/debian-edu-timesyncd.conf \
		share/debian-edu-config/passwords_stub.dat \
		share/debian-edu-config/gosa.conf.template \
		share/debian-edu-config/lightdm-gtk-greeter.conf \
		share/debian-edu-config/sudo-ldap.conf \
		share/debian-edu-config/isc-dhcp-server.service \
		share/debian-edu-config/isc-dhcp-server.service.eth1_only \
		share/pam-configs/edu-group \
		share/pam-configs/edu-umask \
		share/perl5/Debian/Edu.pm \
		share/debian-edu-config/pam-config-ccreds-check \
		share/debian-edu-config/pam-config-ccreds-save \
		share/debian-edu-config/pam-config-mkhomedir \
		share/debian-edu-config/pam-config-nopwdchange \
		share/debian-edu-config/pam-nopwdchange.py \
		share/firefox-esr/distribution/policies.json \
		share/glib-2.0/schemas/21_debian-edu+gdm.gschema.override \
		share/glib-2.0/schemas/31_debian-edu+mate.gschema.override \
		share/glib-2.0/schemas/32-debian-edu.arctica-greeter.gschema.override  \
		share/mate-panel/layouts/debian-edu-mate.layout \
	; do \
		$(INSTALL_DATA) $$f $(DESTDIR)/usr/$$f ; \
	done

# Using manpages autodetection : 
	set -e ; for program in $(LDAPPROGRAMS) ; do \
		$(INSTALL) ldap-tools/$$program $(DESTDIR)$(bindir)/$$program; \
		if [ -e "share/man/man1/$$program.1" ]; \
		then \
		$(INSTALL_DATA) "share/man/man1/$$program.1" $(DESTDIR)$(mandir)/man1/$$program.1 ; \
		fi \
	done
	set -e ; for schema in $(SCHEMAS) ; do \
		$(INSTALL_DATA) ldap-schemas/$$schema $(DESTDIR)$(schemadir)/$$schema;\
	done
	set -e ; for ldif in $(LDIFS) ; do \
		$(INSTALL_DATA) ldap-bootstrap/$$ldif $(DESTDIR)$(ldapdir)/$$ldif ; \
	done

	set -e ; for file in $(WWWFILES) ; do \
		$(INSTALL_DATA) www/$$file $(DESTDIR)/$(wwwdir)/$$file ; \
	done

install-testsuite:
	set -e ; for f in \
		share/debian-edu-config/testsuite-lib.sh \
	; do \
		$(INSTALL_DATA) $$f $(DESTDIR)/usr/$$f ; \
	done

	install -d $(DESTDIR)$(pkglibexecdir)/testsuite
	set -e ; for test in testsuite/* ; do \
		$(INSTALL) $$test $(DESTDIR)$(pkglibexecdir)/$$test; \
	done



clean:
	$(MAKE) -C www $@

dist:
	debuild -us -uc

status:
	$(MAKE) -C www stats

i18n-poke:
	podebconf-report-po
	podebconf-report-po --podir www
